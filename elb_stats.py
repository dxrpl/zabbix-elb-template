#!/usr/bin/python
import datetime
import sys
from optparse import OptionParser
import boto3

### Arguments
parser = OptionParser()
parser.add_option('-i', '--instance-id', dest='instance_id', help='LoadBalancerName')
parser.add_option('-a', '--access-key', dest='access_key', help='AWS Access Key')
parser.add_option('-k', '--secret-key', dest='secret_key', help='AWS Secret Access Key')
parser.add_option('-m', '--metric', dest='metric', help='ELB cloudwatch metric')
parser.add_option('-r', '--region', dest='region', help='AWS region')

options, args = parser.parse_args()

if options.instance_id == None:
  parser.error('LoadBalancerName is required!')
if options.metric == None:
  parser.error('ELB cloudwatch metric is required!')

if not options.access_key or not options.secret_key:
  use_roles = True
else:
  use_roles = False

# Metrics definitions
metrics = {'ActiveConnectionCount':{'type':'int', 'value':None},
  'ClientTLSNegotiationErrorCount':{'type':'int', 'value':None},
  'ConsumedLCUs':{'type':'float', 'value':None},
  'HTTP_Fixed_Response_Count':{'type':'int', 'value':None},
  'HTTP_Redirect_Count':{'type':'int', 'value':None},
  'HTTP_Redirect_Url_Limit_Exceeded_Count':{'type':'int', 'value':None},
  'HTTPCode_ELB_3XX_Count':{'type':'int', 'value':None},
  'HTTPCode_ELB_4XX_Count':{'type':'int', 'value':None},
  'HTTPCode_ELB_5XX_Count':{'type':'int', 'value':None},
  'HTTPCode_ELB_500_Count':{'type':'int', 'value':None},
  'HTTPCode_ELB_502_Count':{'type':'int', 'value':None},
  'HTTPCode_ELB_503_Count':{'type':'int', 'value':None},
  'HTTPCode_ELB_504_Count':{'type':'int', 'value':None},
  'NewConnectionCount':{'type':'int', 'value':None},
  'ProcessedBytes':{'type':'float', 'value':None},
  'RejectedConnectionCount':{'type':'int', 'value':None},
  'RequestCount':{'type':'int', 'value':None},
  'RuleEvaluations':{'type':'int', 'value':None},
}

# Time range
end = datetime.datetime.utcnow()
start = end - datetime.timedelta(minutes=5)

if use_roles:
  conn = boto3.client('cloudwatch', region_name=options.region)
else:
  conn = boto3.client('cloudwatch', aws_access_key_id=options.access_key, aws_secret_access_key=options.secret_key, region_name=options.region)


if options.metric in metrics.keys():
  k = options.metric
  vh = metrics[options.metric]

  try:
    res = conn.get_metric_statistics(Namespace='AWS/ApplicationELB', MetricName=k, Dimensions=[{'Name': 'LoadBalancer', 'Value': options.instance_id}], StartTime=start, EndTime=end, Period=60, Statistics=['Sum'])
  except Exception as e:
    print('Error: Request to AWS CLI failed {}'.format(e))
    sys.exit(1)

  datapoints = res.get('Datapoints')

  if len(datapoints) == 0:
    datapoints = [{'Sum': 0,},] # This is hack because AWS returns empty set if value of some metrics is zero... zabbix doesn't like this.
    #print('Could not find datapoints for specified instance. Please review if provided instance (%s) and region (%s) are correct' % (options.instance_id, options.region))
    #sys.exit(1)

  sum = datapoints[-1].get('Sum')

  if k == 'ProcessedBytes':
    sum = sum / 1024**2 # Change to megabytes
  if vh['type'] == 'float':
    metrics[k]['value'] = '{:.4f}'.format(sum)
  if vh['type'] == 'int':
    metrics[k]['value'] = sum

  print('%s' % (vh['value']))

else:
  print('No such metric')
  sys.exit(1)
