Zabbix ELB Template
===================


This work is massive modification of https://github.com/datorama/zabbix_elb_template which at the time (april 2019) was written in Python 2 an did not work at all.
Many thanks to datorama for basic concepts.


## CAUTION

Do not use `{ITEM.KEY}` macro in any action operations. If you use it e.g. in email notification, then entire key with resolved variables will be sent to your zabbix users as an alert message so they will get your AWS credentials for zabbix user.


## AWS config

1. Create new IAM policy and paste code below to json section:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "cloudwatch:GetMetricStatistics",
            "Resource": "*"
        }
    ]
}
```
Policy name is irrelevant but it's good practise to name it like `zabbix_cloudwatch` or so.

2. Create new user and attach it to newly created policy. And again it's recommended to name it `zabbix` or so.

3. AWS Access Key and AWS Secret Key enter into host macro as described in `Template config` section.


## Template config

1. Place the script `elb_stats.py` at zabbix-server `externalscripts` folder, make sure it has execute permission.
2. Import the template `elb_template.xml` to Zabbix.
3. Create new host and attach the template to it.
4. Set following in host's macro section:
- Instance name `{$INSTANCE_NAME}`. Be aware that ELB instance name is not a DNS name. It looks like: app/load_balancer_name/someDigitsAndNumbers
- AWS credentials `{$AWS_ACCESS_KEY}` and `{$AWS_SECRET_KEY}`
- AWS region `{$REGION}` (only if different than eu-west-1)
